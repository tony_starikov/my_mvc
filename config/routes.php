<?php
/**
 * File returns associative array with routes
 *
 * PHP Version 7
 */

return array(

    'home'=>'site/index', // actionIndex in SiteController

    'posts/([0-9]+)'=>'posts/view/$1', // actionView in PostsController

    'posts'=>'posts/index', // actionIndex in PostsController
);