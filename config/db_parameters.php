<?php
/**
 * File returns associative array with db parameters
 *
 * PHP Version 7
 */

return array(
   'host'=>'localhost',
   'dbname'=>'mvc',
   'user'=>'root',
   'password'=>'root',
);