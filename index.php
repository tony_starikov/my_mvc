<?php
/**
 * Front Controller
 *
 * PHP Version 7
 */

/**
 * COMMON SETTINGS
 */
ini_set('display_errors', 1);
error_reporting(E_ALL);

/**
 * REQUIRING FILES
 */
define('ROOT_DIR', dirname(__FILE__));
require_once(ROOT_DIR.'/components/Router.php');
require_once(ROOT_DIR . '/components/Db.php');

/**
 * ROUTING
 */
$router = new Router();
$router->run();