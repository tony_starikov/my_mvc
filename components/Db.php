<?php

/**
 * Class Db
 *
 * PHP Version 7
 */
class Db
{
    /**
     * Returns connection with database
     *
     * @return PDO
     */
    public static function getConnection()
    {
        $parameters = require_once(ROOT_DIR . '/config/db_parameters.php');

        $dsn = "mysql:host={$parameters['host']};dbname={$parameters['dbname']}";

        return new PDO($dsn, $parameters['user'], $parameters['password']);
    }
}