<?php

/**
 * Class Router
 *
 * PHP Version 7
 */
class Router
{

    /**
     * Associative array with routes
     *
     * @var array
     */
    protected $routes = [];

    /**
     * Router constructor. Add the routes from the config/routes.php
     */
    public function __construct()
    {
        $this->routes = require_once(ROOT_DIR.'/config/routes.php');
    }

    /**
     * Returns request string
     *
     * @return string
     */
    private function getURI()
    {
        if (!empty($_SERVER['REQUEST_URI'])) {
            return trim($_SERVER['REQUEST_URI'], '/');
        }
    }

    /**
     * Match the route to the routes in the routing table
     */
    public function run()
    {
        // Get uri
        $uri = $this->getURI();

        // Try to find this uri in the routes table
        foreach ($this->routes as $uriPattern => $path)
        {
            if (preg_match("~$uriPattern~", $uri)) {

                $internalRoute = preg_replace("~$uriPattern~", $path, $uri);

                $path_parts = explode('/', $internalRoute);

                $controllerName = array_shift($path_parts).'Controller';

                $controllerName = ucfirst($controllerName);

                $actionName = 'action' . ucfirst(array_shift($path_parts));

                $parameters = $path_parts;

                $controllerFile = ROOT_DIR . '/controllers/' . $controllerName . '.php';

                if (file_exists($controllerFile)) {
                    require_once(ROOT_DIR . '/controllers/' . $controllerName . '.php');

                    $controllerObject = new $controllerName;

                    $result = call_user_func_array([$controllerObject, $actionName], $parameters);

                    if ($result != null) {
                        break;
                    }
                }

            }

        }
    }
}