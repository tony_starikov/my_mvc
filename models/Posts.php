<?php
/**
 * Posts model
 *
 * PHP Version 7
 */
class Posts
{
    /**
     * Returns an array of posts items
     *
     * @return array
     */
    public static function getPostsList()
    {
        $db = Db::getConnection();

        $postsList = [];

        $result = $db->query('SELECT * FROM posts ORDER BY created_at DESC LIMIT 10');

        $postsList = $result->fetchAll(PDO::FETCH_ASSOC);

        return $postsList;
    }

    /**
     * Returns single post item specified by id
     *
     * @param integer $id
     * @return array
     */
    public static function getPostById($id)
    {
        $postItem = [];

        if ($id) {
            $db = Db::getConnection();

            $postItem = [];

            $result = $db->query('SELECT * FROM posts WHERE id=' . $id);

            $postItem = $result->fetch(PDO::FETCH_ASSOC);
        }
        return $postItem;
    }
}