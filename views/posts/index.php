<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>All posts</title>
    <link rel="stylesheet" href="/template/css/style.css">
</head>
<body>
    <h1>Show all posts.</h1>

    <div class="main">
        <?php foreach ($postsList as $postItem): ?>
        <div class="post">
            <h2><?=$postItem['title'];?></h2>
            <h3><?=$postItem['created_at'];?></h3>
            <p><?=$postItem['content'];?></p>
            <a href="/posts/<?=$postItem['id'];?>">read</a>
            <hr>
        </div>
        <?php endforeach;?>
    </div>
</body>
</html>