<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Single post</title>
    <link rel="stylesheet" href="/template/css/style.css">
</head>
<body>
    <h1>Show single post.</h1>

    <div class="main">
        <div class="post">
            <h2><?=$postItem['title'];?></h2>
            <h3><?=$postItem['created_at'];?></h3>
            <p><?=$postItem['content'];?></p>
            <hr>
        </div>
    </div>

</body>
</html>