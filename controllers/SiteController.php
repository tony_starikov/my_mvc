<?php
/**
 * Site controller
 *
 * PHP Version 7
 */
class SiteController
{
    /**
     * Show the index page
     */
    public function actionIndex()
    {
        echo 'actionIndex from SiteController class';
        return true;
    }
}