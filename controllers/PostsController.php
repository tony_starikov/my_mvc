<?php

require_once(ROOT_DIR . '/models/Posts.php');

/**
 * Posts controller
 *
 * PHP Version 7
 */
class PostsController
{
    /**
     * Show all posts
     */
    public function actionIndex()
    {
        $postsList = [];
        $postsList = Posts::getPostsList();

        require_once(ROOT_DIR . '/views/posts/index.php');

        return true;
    }

    /**
     * Show single post
     *
     * @param int $id
     * @return bool
     */
    public function actionView($id = 0)
    {
        $id = intval($id);

        if ($id) {
            $postItem = [];

            $postItem = Posts::getPostById($id);

            require_once(ROOT_DIR . '/views/posts/view.php');
        }
        return true;
    }
}